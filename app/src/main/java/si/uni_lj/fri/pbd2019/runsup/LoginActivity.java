package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;

import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class LoginActivity extends AppCompatActivity {

    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;
    private String mUserId;
    private Button signin;
    private int RC_SIGN_IN = 8;
    private String TAG = "LoginActivity";
    private Button weightSet, ageSet, heightSet;
    private TextView weightShow, ageShow, heightShow;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPreferences = this.getSharedPreferences("si.uni_lj.fri.pbd2019.runsup", Context.MODE_PRIVATE);
        weightSet = (Button) findViewById(R.id.login_weight_button);
        ageSet = (Button) findViewById(R.id.login_age_button);
        heightSet = (Button) findViewById(R.id.login_height_button);

        weightShow = (TextView) findViewById(R.id.login_weight_value);
        ageShow = (TextView) findViewById(R.id.login_age_value);
        heightShow = (TextView) findViewById(R.id.login_height_value);


        signin = findViewById(R.id.login_signin);

        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.my_google_api))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
                //signInVirtualUser();

            }
        });

        weightSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText input = new EditText(LoginActivity.this);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(R.string.login_changeweight_label)
                        .setView(input)
                        .setPositiveButton(R.string.login_changeage_save, new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int which) {
                                String sValue = input.getText().toString();
                                int value = Integer.parseInt(sValue);
                                weightShow.setText(sValue + " kg");


                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                                prefs.edit().putInt(Constants.SHARED_USER_WEIGHT, value).apply();

                                Log.d(TAG, "New weight value is: " + value);
                            }
                        })
                        .setNegativeButton(R.string.login_changeage_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .show();
            }
        });

        heightSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText input = new EditText(LoginActivity.this);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(R.string.login_changeheight_label)
                        .setView(input)
                        .setPositiveButton(R.string.login_changeage_save, new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int which) {
                                String sValue = input.getText().toString();
                                int value = Integer.parseInt(sValue);
                                heightShow.setText(sValue + " cm");

                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                                prefs.edit().putInt(Constants.SHARED_USER_HEIGHT, value).apply();

                                Log.d(TAG, "New height value is: " + value);
                            }
                        })
                        .setNegativeButton(R.string.login_changeage_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .show();
            }
        });

        ageSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText input = new EditText(LoginActivity.this);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(R.string.login_changeage_label)
                        .setView(input)
                        .setPositiveButton(R.string.login_changeage_save, new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int which) {
                                String sValue = input.getText().toString();
                                int value = Integer.parseInt(sValue);
                                ageShow.setText(sValue + " years");

                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                                prefs.edit().putInt(Constants.SHARED_USER_AGE, value).apply();

                                Log.d(TAG, "New age value is: " + value);
                            }
                        })
                        .setNegativeButton(R.string.login_changeage_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .show();
            }
        });

        getSharedData();
    }

    private void signIn() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String name = prefs.getString(Constants.SHARED_USER, "Sign in");
        boolean logged = prefs.getBoolean(getString(R.string.userSignedIn), false);

        if(!logged) {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }else{
            mAuth.signOut();

            prefs.edit().putString(Constants.SHARED_USER, "Sign in").apply();
            prefs.edit().putString(Constants.SHARED_USER_PIC, "").apply();
            prefs.edit().putBoolean(getString(R.string.userSignedIn), false).apply();
            prefs.edit().putString(Constants.SHARED_USER_UID, "").apply();

            deleteFromDB();

            signin.setText("Sign in");

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        //Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Snackbar.make(findViewById(R.id.login_activity), "Welcome " + user.getDisplayName(), Snackbar.LENGTH_SHORT).show();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            //Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.login_activity), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void updateUI(FirebaseUser user){
        if(user != null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            String PhotoUrl = user.getPhotoUrl().toString();

            String name = user.getDisplayName();


            final String uid = user.getUid();

            /*user.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (task.isSuccessful()) {
                        String idToken = task.getResult().getToken();


                        // Send token to your backend via HTTPS
                        // ...
                    } else {
                        // Handle error -> task.getException();
                    }
                }
            });
            */
            setUser(uid, "");
            prefs.edit().putBoolean(getString(R.string.userSignedIn), true).apply();
            prefs.edit().putString(Constants.SHARED_USER, name).apply();
            prefs.edit().putString(Constants.SHARED_USER_PIC, PhotoUrl).apply();
            prefs.edit().putString(Constants.SHARED_USER_UID, user.getUid()).apply();

            signin.setText("Sign out");
        }
    }

    public void setUser(String accid, String authToken){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Dao<User, Long> userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        User user = new User(accid);
        user.setAccId(accid);
        user.setAuthToken(authToken);
        try {
            userDao = databaseHelper.userDao();
            userDao.create(user);
            prefs.edit().putString(getString(R.string.userId), accid).apply();
            Log.d(TAG, "user loggied with id " + user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteFromDB(){
        Dao<User, Long> userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        try {
            userDao = databaseHelper.userDao();

            DeleteBuilder<User, Long> deleteBuilder = userDao.deleteBuilder();
            userDao.delete(deleteBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void getSharedData(){
        int age = mSharedPreferences.getInt(Constants.SHARED_USER_AGE, 25);
        int weight = mSharedPreferences.getInt(Constants.SHARED_USER_WEIGHT, 80);
        int height = mSharedPreferences.getInt(Constants.SHARED_USER_HEIGHT, 180);

        weightShow.setText(Integer.toString(weight));
        heightShow.setText(Integer.toString(height));
        ageShow.setText(Integer.toString(age));
    }

    public void signInVirtualUser(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        try{
            Dao<User, Long> userLongDao;
            User user = new User ("fdafd");
            userLongDao = databaseHelper.userDao();
            userLongDao.create(user);
            editor.putBoolean("userSignedIn", true).apply();
            editor.putLong("userId", user.getId()).apply();
            Toast.makeText(this, "Virtual user signed.", Toast.LENGTH_SHORT).show();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
