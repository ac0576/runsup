package si.uni_lj.fri.pbd2019.runsup;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.location.Location;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.ContentFrameLayout;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Date;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class WorkoutDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private int activity = 0;
    private long durationValue = 0;
    private double distanceValue = 0;
    private double paceValue = 0;
    private double caloriesValue = 0;
    private long workoutId = 0;
    private String title;
    private ArrayList<List<Location>> finalPositionList ;

    private TextView titleView;
    private TextView activityType;
    private TextView duration;
    private TextView date;
    private TextView calories;
    private TextView distance;
    private TextView pace;

    private Button shareFb;
    private Button shareGoogle;
    private Button shareTwitter;
    private Button sendMail;
    private Button viewMap;

    private EditText shareMsg;

    private GoogleMap mMap;
    private boolean kmLabel = true;

    private static final String TAG = "WorkoutDetailActivity";

    private Workout curWorkout;

    private long parserId = -1;

    private ContentFrameLayout mapLayout;

    private DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
            DatabaseHelper.class);

    private Hashtable<Integer, String> activities = new Hashtable<Integer, String>()
    {{
        put(0, "Running");
        put(1, "Walking");
        put(2, "Cycling");
    }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_workoutdetail_map);
        mapFragment.getMapAsync(this);


        titleView = (TextView) findViewById(R.id.textview_workoutdetail_workouttitle);
        activityType = (TextView) findViewById(R.id.textview_workoutdetail_sportactivity);
        duration = (TextView) findViewById(R.id.textview_workoutdetail_valueduration);
        calories = (TextView) findViewById(R.id.textview_workoutdetail_valuecalories);
        distance = (TextView) findViewById(R.id.textview_workoutdetail_valuedistance);
        pace = (TextView) findViewById(R.id.textview_workoutdetail_valueavgpace);
        date = (TextView) findViewById(R.id.textview_workoutdetail_activitydate);


        shareFb = (Button) findViewById(R.id.button_workoutdetail_fbsharebtn);
        shareTwitter = (Button) findViewById(R.id.button_workoutdetail_twittershare);
        shareGoogle = (Button) findViewById(R.id.button_workoutdetail_gplusshare);
        sendMail = (Button) findViewById(R.id.button_workoutdetail_emailshare);
        viewMap = (Button) findViewById(R.id.button_workoutdetail_showmap);

        shareMsg = (EditText) findViewById(R.id.share_message);

        mapLayout = (ContentFrameLayout) findViewById(R.id.contentFrameLayout3);

        getCurrentWorkout();

        shareFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareMsg.setVisibility(View.VISIBLE);
            }
        });

        shareTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareMsg.setVisibility(View.VISIBLE);
                shareMsg.requestFocus();
            }
        });

        shareGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareMsg.setVisibility(View.VISIBLE);
                shareMsg.requestFocus();
            }
        });

        sendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareMsg.setVisibility(View.VISIBLE);
                shareMsg.requestFocus();
            }
        });

        titleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText input = new EditText(WorkoutDetailActivity.this);
                input.setText(titleView.getText());
                new AlertDialog.Builder(WorkoutDetailActivity.this)
                        .setTitle(R.string.workout_title_change)
                        .setView(input)
                        .setPositiveButton(R.string.login_changeage_save, new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int which) {
                                String sValue = input.getText().toString();
                                titleView.setText(sValue);
                                updateDB();

                                Log.d(TAG, "New title value is: " + sValue);
                            }
                        })
                        .setNegativeButton(R.string.login_changeage_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .show();
            }
        });

        viewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showFullMap = new Intent(WorkoutDetailActivity.this, MapsActivity.class);
                showFullMap.putExtra("workoutId", workoutId);
                startActivity(showFullMap);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        kmLabel = loadSavedPreferencesLable();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        //Intent setIntent = new Intent(this, MainActivity.class);
        //startActivity(setIntent);
        if(parserId != -1) {
            deleteWorkout();
            deletePoints();
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.workoutdetails_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent in = new Intent(this, SettingsActivity.class);
            startActivity(in);
            return true;
        }else if(id == R.id.nav_delete){
            deleteWorkout();
            deletePoints();
            if(parserId != -1) {
                deleteFromParser();
                deleteGpsParser();
            }
            Intent setIntent = new Intent(this, MainActivity.class);
            startActivity(setIntent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void deleteWorkout(){
        //workoutId

        Dao<Workout, Long> workoutLongDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        try {
            workoutLongDao = databaseHelper.workoutDao();

            DeleteBuilder<Workout, Long> deleteBuilder = workoutLongDao.deleteBuilder();
            deleteBuilder.where().eq("id", workoutId);
            workoutLongDao.delete(deleteBuilder.prepare());
            Log.d(TAG, "Current workout deleted.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateDB(){
        Dao<Workout, Long> workoutLongDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        try {
            workoutLongDao = databaseHelper.workoutDao();
            UpdateBuilder<Workout, Long> updateBuilder = workoutLongDao.updateBuilder();
            updateBuilder.where().eq("id", workoutId);
            updateBuilder.updateColumnValue("title", titleView.getText().toString());
            updateBuilder.update();
            Log.d(TAG, "Title changed.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getCurrentWorkout(){
        Intent intent = getIntent();
        workoutId = intent.getLongExtra("workoutId", -1);
        parserId = intent.getLongExtra("parserId", -1);

        Dao<Workout, Long> workoutLongDao = null;
        try {
            workoutLongDao = databaseHelper.workoutDao();
            QueryBuilder<Workout, Long> builder = workoutLongDao.queryBuilder();
            builder.where().eq("id", workoutId);
            List<Workout> list = workoutLongDao.query(builder.prepare());  // returns list of ten items
            if (list.size()>0) {
                curWorkout = list.get(0);
                setCurWorkout();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setCurWorkout(){
        //Prepare values
        kmLabel = loadSavedPreferencesLable();
        Log.d(TAG, "label: " + kmLabel);

        activity = curWorkout.getSportActivity();
        durationValue = curWorkout.getDuration() ;
        distanceValue = curWorkout.getDistance();
        paceValue = curWorkout.getPaceAvg();
        caloriesValue = curWorkout.getTotalCalories();
        title = curWorkout.getTitle();
        //finalPositionList = (ArrayList) intent.getParcelableArrayListExtra("finalPostitionList");

        Log.d(TAG, "Final Durration: " + durationValue + " Formated: " + MainHelper.formatDuration(durationValue));
        //Set values
        titleView.setText(title);
        activityType.setText(activities.get(activity));
        String text;
        text = MainHelper.formatDuration(durationValue);
        duration.setText(text);
        if(kmLabel) {
            text = MainHelper.formatDistance(distanceValue);
            distance.setText(text + " km");
        }else {
            text = MainHelper.formatDistance(MainHelper.kmToMi(distanceValue));
            distance.setText(text + " mi");
        }
        if(kmLabel) {
            text = MainHelper.formatPace(paceValue);
            text = text + " min/km";
        }else {
            text = MainHelper.formatPace(MainHelper.minpkmToMinpmi(paceValue));
            text = text + " min/mi";
        }
        pace.setText(text);
        text = MainHelper.formatCalories(caloriesValue);
        calories.setText(text + " kcal");
        date.setText(SimpleDateFormat.getDateTimeInstance().format(new Date()));
        String asd = "I was out for " + activities.get(activity) + ". I did " + MainHelper.formatDistance(distanceValue) + " km in " + MainHelper.formatDuration(durationValue);
        shareMsg.setText(asd);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent openFullMap = new Intent(WorkoutDetailActivity.this, MapsActivity.class);
                openFullMap.putExtra("workoutId", workoutId);
                startActivity(openFullMap);
            }
        });

        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        mMap = googleMap;
        long curSes = 0;
        int tmpi = 0;

        List<GpsPoint> list = getLastPoint();
        if(list == null) {
            mapLayout.setVisibility(View.GONE);
            viewMap.setVisibility(View.GONE);
            return;
        }else if(list.size() <= 1){
            mapLayout.setVisibility(View.GONE);
            viewMap.setVisibility(View.GONE);
            return;
        }

        if(list.size()>1) {
            for (int i = 0; i < list.size(); i++) {
                if(i == 0 ){
                    curSes = list.get(i).getSessionNumber();
                    tmpi = 0;
                }
                LatLng point = new LatLng(list.get(i).getLatitude(), list.get(i).getLongitude());
                builder.include(point);
                mMap.addMarker(new MarkerOptions().position(point));

                if (i > 0 && curSes == list.get(i).getSessionNumber() && tmpi != i) {
                    curSes = list.get(i).getSessionNumber();
                    LatLng point1 = new LatLng(list.get(i - 1).getLatitude(), list.get(i - 1).getLongitude());
                    mMap.addPolyline(new PolylineOptions()
                            .color(Color.RED)
                            .add(
                                    point1,
                                    point
                            ));
                }else if(curSes != list.get(i).getSessionNumber()){
                    curSes = list.get(i).getSessionNumber();
                    tmpi = i;
                }
            }

            final LatLngBounds bounds = builder.build();
            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 30));
                }
            });

        }

    }

    public List<GpsPoint> getLastPoint(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        Dao<GpsPoint, Long> gpsPointLongDao = null;
        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            QueryBuilder<GpsPoint, Long> builder = gpsPointLongDao.queryBuilder();
            builder.where().eq("workout_id", workoutId);
            builder.orderBy("created", true);  // true for ascending, false for descending
            List<GpsPoint> list = gpsPointLongDao.query(builder.prepare());  // returns list of ten items
            if(list.size() > 1)
                return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean loadSavedPreferencesLable() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String unit = sp.getString("unit", "0");
        //Log.d(TAG, "Got from sp: " + unit);
        if(unit.equals("0"))
            return true;
        else
            return false;
    }

    public void deletePoints(){
        //workoutId

        Dao<GpsPoint, Long> gpsPointLongDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();

            DeleteBuilder<GpsPoint, Long> deleteBuilder = gpsPointLongDao.deleteBuilder();
            deleteBuilder.where().eq("workout_id", workoutId);
            gpsPointLongDao.delete(deleteBuilder.prepare());
            Log.d(TAG, "Current Gps points deleted.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deleteFromParser() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Workout");

        query.whereEqualTo("id", parserId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> object, ParseException e) {
                if (e == null) {
                    //Delete based on the position
                    Log.d(TAG, "Got parser workouts: " + object.size() + " with id: " + parserId);
                    for(int i = 0; i<object.size(); i++) {
                        object.get(i).deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {

                                if (e == null) {

                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            e.getMessage().toString(),
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            e.getMessage().toString(),
                            Toast.LENGTH_LONG
                    ).show();
                }
            };
        });
    }

    private void deleteGpsParser() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GpsPoint");

        query.whereEqualTo("workout_id", parserId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> object, ParseException e) {
                if (e == null) {
                    for(int i = 0; i<object.size(); i++) {
                        object.get(i).deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {

                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            e.getMessage().toString(),
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            e.getMessage().toString(),
                            Toast.LENGTH_LONG
                    ).show();
                }
            };
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(parserId != -1) {
            deleteWorkout();
            deletePoints();
        }
    }
}
