package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ActiveWorkoutMapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ActiveWorkoutMapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActiveWorkoutMapFragment extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ActiveWorkoutMapFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private GoogleMap mMap;
    private long workoutId;
    private GpsPoint curPoint;
    private Handler mHandler = new Handler();
    private ArrayList<GpsPoint> listOfPoints = new ArrayList<GpsPoint>();
    private int lineCounter = 0;

    BroadcastReceiver broadcastReceiver;


    public ActiveWorkoutMapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ActiveWorkoutMapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ActiveWorkoutMapFragment newInstance(String param1, String param2) {
        ActiveWorkoutMapFragment fragment = new ActiveWorkoutMapFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Constants.TICK)){
                    workoutId = intent.getLongExtra("workoutId", -1);
                }
            }
        };

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_active_workout_map, container, false);
        workoutId = -1;
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.f_fragment_activeworkoutmap_map);
        mapFragment.getMapAsync(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.TICK);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, filter);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private Runnable getPoints = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(getPoints, 1000);
            if(workoutId != -1) {
                getAndSetPoints();
                if (lineCounter == 15) {
                    drawPath();
                }
                lineCounter++;
            }
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mHandler.post(getPoints);

        // Add a marker in Sydney, Australia, and move the camera.
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);

    }

    public void getAndSetPoints(){
        GpsPoint tmpPoint = getLastPoint();

        if (tmpPoint != null){
            Log.d(TAG, "Long: " + tmpPoint.getLongitude());
            if(tmpPoint != curPoint){
                curPoint = tmpPoint;
                listOfPoints.add(curPoint);
                //listOfPoints.add(new LatLng(curPoint.getLatitude(), curPoint.getLongitude()));
                LatLng point = new LatLng(curPoint.getLatitude(), curPoint.getLongitude());
                mMap.addMarker(new MarkerOptions().position(point));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
                mMap.setMinZoomPreference(17);
            }
        }
    }

    private GpsPoint getLastPoint(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        Dao<GpsPoint, Long> gpsPointLongDao = null;
        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            QueryBuilder<GpsPoint, Long> builder = gpsPointLongDao.queryBuilder();
            builder.where().eq("workout_id", workoutId);
            builder.orderBy("created", false);  // true for ascending, false for descending
            List<GpsPoint> list = gpsPointLongDao.query(builder.prepare());  // returns list of ten items
            Log.d(TAG, "List size: " + list.size());
            if(list.size() > 0)
                return list.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    private void drawPath(){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        lineCounter = 0;
        long curSes = 0;
        int tmpi = 0;

        if(listOfPoints.size()>1) {
            for (int i = 1; i < listOfPoints.size(); i++) {
                if(i == 0 ){
                    curSes = listOfPoints.get(i).getSessionNumber();
                    tmpi = 0;
                }

                LatLng point = new LatLng(listOfPoints.get(i).getLatitude(), listOfPoints.get(i).getLongitude());
                builder.include(point);
                mMap.addMarker(new MarkerOptions().position(point));

                if (i > 0 && curSes == listOfPoints.get(i).getSessionNumber() && tmpi != i) {
                    curSes = listOfPoints.get(i).getSessionNumber();
                    LatLng point1 = new LatLng(listOfPoints.get(i - 1).getLatitude(), listOfPoints.get(i - 1).getLongitude());
                    mMap.addPolyline(new PolylineOptions()
                            .color(Color.RED)
                            .add(
                                    point1,
                                    point
                            ));
                }else if(curSes != listOfPoints.get(i).getSessionNumber()){
                    curSes = listOfPoints.get(i).getSessionNumber();
                    tmpi = i;
                }
            }

            GpsPoint tmp = listOfPoints.get(listOfPoints.size() - 1);
            listOfPoints.removeAll(listOfPoints);
            listOfPoints.add(tmp);
        }
    }
}
