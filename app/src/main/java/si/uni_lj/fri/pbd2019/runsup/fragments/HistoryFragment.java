package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.HistoryListAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HistoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {

    private static final String TAG = "HistoryFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private ListView hisoryList;
    private List<Workout> list  = new ArrayList<Workout>();
    private SwipeRefreshLayout pullToRefresh;
    private boolean loggedin = false;
    private TextView noHistoryText;
    private ArrayList<Workout> paserList = null;
    private ArrayList<GpsPoint> gpsPointsAll = null;
    private SharedPreferences preferences;

    private OnFragmentInteractionListener mListener;

    public HistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        startParser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_history, container, false);
        hisoryList = v.findViewById(R.id.listview_history_workouts);
        noHistoryText = v.findViewById(R.id.textview_history_noHistoryData);
        pullToRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        getPreferences();

        setHasOptionsMenu(true);

        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()).getApplicationContext());

        String name = preferences.getString(Constants.SHARED_USER, "Sign in");
        loggedin = preferences.getBoolean(getString(R.string.userSignedIn), false);


        //getData();
        if(loggedin) {
            syncParser();
            deleteLocal();
        }else{
            updateAdapter();
        }
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(loggedin) {
                    syncParser();
                    deleteLocal();
                }else
                    updateAdapter();
                pullToRefresh.setRefreshing(false);
            }
        });

        hisoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Workout selectedWorkout = (Workout) parent.getItemAtPosition(position);
                long parserId = selectedWorkout.getId();
                Workout curW = putWorkoutToDb(selectedWorkout);
                Log.d(TAG, "Selected workout " + selectedWorkout.getId() + " other" + curW.getId());
                for(int i=0; i<gpsPointsAll.size(); i++) {
                    if(selectedWorkout.getId() == gpsPointsAll.get(i).getWorkout().getId())
                        saveLocally(gpsPointsAll.get(i), curW);
                }

                Intent displaySelected = new Intent(getContext(), WorkoutDetailActivity.class);
                displaySelected.putExtra("workoutId", curW.getId());
                displaySelected.putExtra("parserId", parserId);
                getActivity().startActivity(displaySelected);
            }
        });


        //Wokrout [] wor = ..
        //Get workouts and place them
        //ListAdapter adapter = new HistoryListAdapter(getContext(), wor);
        //hisoryList.setAdapter(adapter);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.history_menu, menu);
        MenuItem item = menu.findItem(R.id.action_sync);
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()).getApplicationContext());
        /*Map<String, ?> allEntries = prefs.getAll();
        Log.d(TAG, "Got " + allEntries.size() + " preferences.");
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d(TAG,  entry.getKey() + ": " + entry.getValue().toString());
        }
        */
        String name = preferences.getString(Constants.SHARED_USER, "Sign in");
        loggedin = preferences.getBoolean(getString(R.string.userSignedIn), false);
        boolean asds = preferences.getBoolean("userSignedIn", false);

        Log.d(TAG, "Global " + loggedin + " my " + asds);
        if(loggedin)
            item.setVisible(true);
        else
            item.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.all_actiondelete){
            new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.clear_history_conformation)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which) {
                            deleteLocal();
                            if(loggedin)
                                deleteObject();
                            //getData();
                            if(!loggedin)
                                updateAdapter();
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    })
                    .show();
        }else if(item.getItemId() == R.id.action_sync){
            list = new ArrayList<>();
            getFromParser();
            //updateAdapter();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getData(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        Dao<Workout, Long> workoutLongDao = null;
        try {
            workoutLongDao = databaseHelper.workoutDao();
            //gpsPointDao.create(curGpsPoint);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        QueryBuilder<Workout, Long> builder = workoutLongDao.queryBuilder();
        //builder.orderBy("duration", true);  // true for ascending, false for descending
        try {
            list = workoutLongDao.query(builder.prepare());  // returns list of ten items
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void startParser(){
        Parse.initialize(new Parse.Configuration.Builder(getContext())
                .applicationId(getString(R.string.back4app_app_id))
                // if defined
                .clientKey(getString(R.string.back4app_client_key))
                .server(getString(R.string.back4app_server_url))
                .build()
        );

        Log.d("HistoryFragment", "Parser started");
    }

    public void syncParser(){
        Log.d(TAG, "Sync initialized.");
        list = new ArrayList<Workout>();
        List<Workout> workoutsByUser = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        Dao<Workout, Long> workoutLongDao = null;


        try {
            workoutLongDao = databaseHelper.workoutDao();
            QueryBuilder<Workout, Long> builder = workoutLongDao.queryBuilder();
            User tmp = getLoggedUser();

            builder.where().eq("user_id", tmp.getId());
            builder.where().eq("finished", true);
            workoutsByUser = workoutLongDao.query(builder.prepare());  // returns list of ten items
            if(workoutsByUser.size()>0){
                Log.d(TAG, workoutsByUser.get(0).getTitle()+ "");
            }else{
                getFromParser();
            }
            for(int i=0; i<workoutsByUser.size(); i++) {
                ParseObject workoutList = new ParseObject("Workout");
                workoutList.put("id", workoutsByUser.get(i).getId());
                workoutList.put("user", tmp.getAccId());
                String title = workoutsByUser.get(i).getTitle();
                if(title == null)
                    title = "";
                workoutList.put("title", title);
                workoutList.put("created", workoutsByUser.get(i).getCreated());
                workoutList.put("status", workoutsByUser.get(i).getStatus());
                workoutList.put("distance", workoutsByUser.get(i).getDistance());
                workoutList.put("duration", workoutsByUser.get(i).getDuration());
                workoutList.put("totalCalories", workoutsByUser.get(i).getTotalCalories());
                workoutList.put("paceAvg", workoutsByUser.get(i).getPaceAvg());
                workoutList.put("sportActivity", workoutsByUser.get(i).getSportActivity());
                workoutList.put("lastUpdate", workoutsByUser.get(i).getLastUpdate());
                syncGpsPoints(workoutsByUser.get(i));

                boolean show = false;
                if(i == workoutsByUser.size()-1)
                    show = true;
                final boolean finalShow = show;
                workoutList.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        if (e == null) {
                            Log.d(TAG, "all OK");
                            deleteLocal();
                            if(finalShow)
                                getFromParser();

                        } else {
                            Log.d(TAG, "error saving " + e.getMessage().toString());

                            Toast.makeText(
                                    getActivity().getApplicationContext(),
                                    e.getMessage().toString(),
                                    Toast.LENGTH_LONG
                            ).show();
                        }
                    }
                });
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public User getLoggedUser(){
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()).getApplicationContext());

        //String ACCID = prefs.getString(Constants.SHARED_USER_UID, "77777");
        String ACCID = preferences.getString(getString(R.string.userId), "");
        Log.d(TAG, "user shared id is: " + ACCID);
        List<User> users = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        Dao<User, Long> userLongDao = null;


        try {
            userLongDao = databaseHelper.userDao();
            QueryBuilder<User, Long> builder = userLongDao.queryBuilder();
            builder.where().eq("accId", ACCID);
            users = userLongDao.query(builder.prepare());  // returns list of ten items
            Log.d(TAG, "Got n users: " + users.size());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        if(users.size() > 0) {

            return users.get(0);
        }else
            return null;
    }

    public void deleteLocal(){
        Dao<Workout, Long> workoutLongDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        try {
            workoutLongDao = databaseHelper.workoutDao();
            DeleteBuilder<Workout, Long> deleteBuilder = workoutLongDao.deleteBuilder();
            deleteBuilder.where().eq("finished", true);
            workoutLongDao.delete(deleteBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void getFromParser(){
        if(getContext()!=null)
            Toast.makeText(getContext(), "Getting data, please wait.", Toast.LENGTH_SHORT).show();
        paserList = new ArrayList<Workout>();

        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()).getApplicationContext());


        String ACCID = preferences.getString(getString(R.string.userId), "");

        Log.d(TAG, "Users accid: " + ACCID);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Workout");

        // Query Parameters
        //query.whereEqualTo("userId", ParseUser.getCurrentUser());

        // Sorts the results in ascending order by the itemName field
        query.orderByDescending("created");
        query.whereEqualTo("user", ACCID);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, final ParseException e) {
                if (e == null){
                    // Adding objects into the Array
                    for(int i= 0 ; i < objects.size(); i++){
                        Workout tmpW = new Workout();
                        tmpW.setPaceAvg(objects.get(i).getDouble("paceAvg"));
                        tmpW.setTitle(objects.get(i).getString("title"));
                        tmpW.setDistance(objects.get(i).getDouble("distance"));
                        tmpW.setDuration(objects.get(i).getLong("duration"));
                        tmpW.setStatus(objects.get(i).getInt("status"));
                        tmpW.setStatus(objects.get(i).getInt("status"));
                        tmpW.setCreated(objects.get(i).getDate("created"));
                        tmpW.setTotalCalories(objects.get(i).getInt("totalCalories"));
                        tmpW.setSportActivity(objects.get(i).getInt("sportActivity"));
                        tmpW.setLastUpdate(objects.get(i).getDate("lastUpdate"));
                        tmpW.setId(objects.get(i).getLong("id"));
                        tmpW.setUser(getLoggedUser());
                        getFromParserGps(tmpW);
                        list.add(tmpW);
                    }
                    Log.d(TAG, "List length from parser: " + list.size());
                    updateAdapter();

                } else {

                }
            }
        });
    }

    public void updateAdapter(){
        Log.d(TAG, "List length from all: " + list.size());
        if(list != null && list.size()>0) {
            hisoryList.setVisibility(View.VISIBLE);
            noHistoryText.setVisibility(View.GONE);
            //Log.d(TAG, list.get(0).getUser());
            ListAdapter adapter = new HistoryListAdapter(getContext(), list);
            hisoryList.setAdapter(adapter);
        }else{
            hisoryList.setVisibility(View.GONE);
            noHistoryText.setVisibility(View.VISIBLE);
        }
    }

    private void deleteObject() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Workout");

        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()).getApplicationContext());

        //String ACCID = prefs.getString(Constants.SHARED_USER_UID, "77777");
        String ACCID = preferences.getString(getString(R.string.userId), "");

        query.whereEqualTo("user", ACCID);
        query.orderByAscending("created");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> object, ParseException e) {
                if (e == null) {
                    //Delete based on the position
                    for(int i = 0; i<object.size(); i++) {
                        long id  = object.get(i).getLong("id");
                        deleteGpsParser(id);
                        boolean tmp = false;
                        if(i == object.size()-1)
                            tmp = true;
                        final boolean refresh = tmp;
                        object.get(i).deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {

                                if (e == null) {
                                    if(refresh) {
                                        list = new ArrayList<>();
                                        getFromParser();
                                        Log.d(TAG, "Refreshing.");
                                    }
                                } else {
                                    Toast.makeText(
                                            getActivity().getApplicationContext(),
                                            e.getMessage().toString(),
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(
                            getActivity().getApplicationContext(),
                            e.getMessage().toString(),
                            Toast.LENGTH_LONG
                    ).show();
                }
            };
        });
    }

    public Workout putWorkoutToDb(Workout workout){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        Dao<Workout, Long> workoutDao = null;
        try {
            workoutDao = databaseHelper.workoutDao();
            workoutDao.create(workout);
            return workout;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void syncGpsPoints(Workout workout_id){
        List<GpsPoint> gpsList = new ArrayList<GpsPoint>();
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        Dao<GpsPoint, Long> gpsPointLongDao = null;


        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            QueryBuilder<GpsPoint, Long> builder = gpsPointLongDao.queryBuilder();
            builder.where().eq("workout_id", workout_id);
            gpsList = gpsPointLongDao.query(builder.prepare());  // returns list of ten items

            for(int i=0; i<gpsList.size(); i++) {
                ParseObject gpsPoint = new ParseObject("GpsPoint");
                gpsPoint.put("id", gpsList.get(i).getId());
                gpsPoint.put("workout_id", workout_id.getId());
                gpsPoint.put("sessionNumber", gpsList.get(i).getSessionNumber());
                gpsPoint.put("latitude", gpsList.get(i).getLatitude());
                gpsPoint.put("longitude", gpsList.get(i).getLongitude());
                gpsPoint.put("duration", gpsList.get(i).getDuration());
                gpsPoint.put("speed", gpsList.get(i).getSpeed());
                gpsPoint.put("pace", gpsList.get(i).getPace());
                gpsPoint.put("totalCalories", gpsList.get(i).getTotalCalories());
                gpsPoint.put("created", gpsList.get(i).getCreated());
                gpsPoint.put("lastUpdate", gpsList.get(i).getLastUpdate());
                gpsPoint.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        if (e == null) {
                            Log.d(TAG, "Gps points saved.");
                            deleteLocalGPSp();

                        } else {
                            Log.d(TAG, "error saving GPS points: " + e.getMessage().toString());

                            Toast.makeText(
                                    getActivity().getApplicationContext(),
                                    e.getMessage().toString(),
                                    Toast.LENGTH_LONG
                            ).show();
                        }
                    }
                });
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteLocalGPSp(){
        Dao<GpsPoint, Long> gpsPointLongDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            DeleteBuilder<GpsPoint, Long> deleteBuilder = gpsPointLongDao.deleteBuilder();
            gpsPointLongDao.delete(deleteBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void deleteGpsParser(Long workout_id) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GpsPoint");

        query.whereEqualTo("workout_id", workout_id);
        Log.d(TAG, "Id to delete from: " + workout_id);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> object, ParseException e) {
                if (e == null) {
                    //Delete based on the position
                    //Log.d(TAG, "Found " + object.size() + " Gps points to delete.");
                    for(int i = 0; i<object.size(); i++) {
                        object.get(i).deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {

                                } else {
                                    Toast.makeText(
                                            getActivity().getApplicationContext(),
                                            e.getMessage().toString(),
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(
                            getActivity().getApplicationContext(),
                            e.getMessage().toString(),
                            Toast.LENGTH_LONG
                    ).show();
                }
            };
        });
    }

    public void getFromParserGps(final Workout sW){
        gpsPointsAll = new ArrayList<GpsPoint>();
        Log.d(TAG, "Workout id for Getting GPS: " + sW.getId());

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GpsPoint");
        query.whereEqualTo("workout_id", sW.getId());

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, final ParseException e) {
                if (e == null){
                    // Adding objects into the Array
                    for(int i= 0 ; i < objects.size(); i++){
                        GpsPoint tmpW = new GpsPoint();
                        tmpW.setId(objects.get(i).getLong("id"));
                        tmpW.setWorkout(sW);
                        tmpW.setSessionNumber(objects.get(i).getLong("sessionNumber"));
                        tmpW.setDuration(objects.get(i).getLong("duration"));
                        tmpW.setLatitude(objects.get(i).getDouble("latitude"));
                        tmpW.setLongitude(objects.get(i).getDouble("longitude"));
                        tmpW.setCreated(objects.get(i).getDate("created"));
                        tmpW.setTotalCalories(objects.get(i).getDouble("totalCalories"));
                        tmpW.setSpeed((float) objects.get(i).getDouble("speed"));
                        tmpW.setLastUpdate(objects.get(i).getDate("lastUpdate"));
                        tmpW.setPace(objects.get(i).getDouble("pace"));
                        gpsPointsAll.add(tmpW);
                    }
                    Log.d(TAG, "List length from parser GPS: " + gpsPointsAll.size());
                    updateAdapter();

                } else {

                }
            }
        });
    }

    public void saveLocally(GpsPoint gp, Workout curW){
        gp.setWorkout(curW);
        Dao<GpsPoint, Long> gpsPointDao = null;

        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        try {
            gpsPointDao = databaseHelper.gpsPointDao();
            gpsPointDao.create(gp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void getPreferences(){
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }
}
