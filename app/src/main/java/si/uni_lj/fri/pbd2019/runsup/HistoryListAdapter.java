package si.uni_lj.fri.pbd2019.runsup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.icu.text.SimpleDateFormat;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class HistoryListAdapter extends ArrayAdapter<Workout> {

    private boolean kmLabel = true;

    public HistoryListAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List objects) {
        super(context, resource, textViewResourceId, objects);

    }

    public HistoryListAdapter(Context context, List<Workout> obj){
        super(context, R.layout.adapter_history, obj);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        @SuppressLint("ViewHolder") View v = inflater.inflate(R.layout.adapter_history, parent, false);

        kmLabel = loadSavedPreferencesLable();

        ImageView image = v.findViewById(R.id.imageview_history_icon);
        TextView title = v.findViewById(R.id.textview_history_title);
        TextView date = v.findViewById(R.id.textview_history_datetime);
        TextView activity = v.findViewById(R.id.textview_history_sportactivity);

        //if(getItem(position).getImg() != null)
        //    image.setImageBitmap(getItem(position).getImg());
        //else
            image.setImageResource(R.mipmap.ic_launcher_round);
        title.setText(getItem(position).getTitle());
        date.setText(SimpleDateFormat.getDateTimeInstance().format(getItem(position).getCreated()));
        if (kmLabel)
            activity.setText(" " + MainHelper.formatDuration(getItem(position).getDuration())
                    + " " + MainHelper.activity.get(getItem(position).getSportActivity())
                    + " | " + MainHelper.formatDistance(getItem(position).getDistance())
                    + " km | " + MainHelper.formatCalories(getItem(position).getTotalCalories())
                    + " kcal | avg "
                    + MainHelper.formatPace(getItem(position).getPaceAvg()) + " min/km");
        else
            activity.setText(" " + MainHelper.formatDuration(getItem(position).getDuration())
                    + " " + MainHelper.activity.get(getItem(position).getSportActivity())
                    + " | " + MainHelper.formatDistance(MainHelper.kmToMi(getItem(position).getDistance()))
                    + " mi | " + MainHelper.formatCalories(getItem(position).getTotalCalories())
                    + " kcal | avg "
                    + MainHelper.formatPace(MainHelper.minpkmToMinpmi(getItem(position).getPaceAvg()))
                    + " min/mi");
        return v;
    }

    public boolean loadSavedPreferencesLable() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        String unit = sp.getString("unit", "0");
        if(unit.equals("0"))
            return true;
        else
            return false;
    }
}
