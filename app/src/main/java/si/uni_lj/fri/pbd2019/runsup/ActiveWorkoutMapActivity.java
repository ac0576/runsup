package si.uni_lj.fri.pbd2019.runsup;

import android.graphics.Color;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class ActiveWorkoutMapActivity extends AppCompatActivity  implements OnMapReadyCallback {

    private Button back;
    private GoogleMap mMap;
    private long workoutId;
    private GpsPoint curPoint;
    private Handler mHandler = new Handler();
    private ArrayList<GpsPoint> listOfPoints = new ArrayList<GpsPoint>();
    private int lineCounter = 0;

    private Runnable getPoints = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(getPoints, 1000);
            getAndSetPoints();
            if(lineCounter == 15){
                drawPath();
            }
            lineCounter++;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_workout_map);

        workoutId = getIntent().getLongExtra("workoutId", -1);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_activeworkoutmap_map);
        mapFragment.getMapAsync(this);

        back = (Button) findViewById(R.id.button_activeworkoutmap_back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mHandler.post(getPoints);

        // Add a marker in Sydney, Australia, and move the camera.
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);

    }

    public void getAndSetPoints(){
        GpsPoint tmpPoint = getLastPoint();

        if (tmpPoint != null){
            if(tmpPoint != curPoint){
                curPoint = tmpPoint;
                listOfPoints.add(curPoint);
                //listOfPoints.add(new LatLng(curPoint.getLatitude(), curPoint.getLongitude()));
                LatLng point = new LatLng(curPoint.getLatitude(), curPoint.getLongitude());
                mMap.addMarker(new MarkerOptions().position(point));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
                mMap.setMinZoomPreference(17);
            }
        }
    }

    public GpsPoint getLastPoint(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        Dao<GpsPoint, Long> gpsPointLongDao = null;
        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            QueryBuilder<GpsPoint, Long> builder = gpsPointLongDao.queryBuilder();
            builder.where().eq("workout_id", workoutId);
            builder.orderBy("created", false);  // true for ascending, false for descending
            List<GpsPoint> list = gpsPointLongDao.query(builder.prepare());  // returns list of ten items
            if(list.size() > 0)
                return list.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    public void drawPath(){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        lineCounter = 0;
        long curSes = 0;
        int tmpi = 0;

        if(listOfPoints.size()>1) {
            for (int i = 1; i < listOfPoints.size(); i++) {
                if(i == 0 ){
                    curSes = listOfPoints.get(i).getSessionNumber();
                    tmpi = 0;
                }

                LatLng point = new LatLng(listOfPoints.get(i).getLatitude(), listOfPoints.get(i).getLongitude());
                builder.include(point);
                mMap.addMarker(new MarkerOptions().position(point));

                if (i > 0 && curSes == listOfPoints.get(i).getSessionNumber() && tmpi != i) {
                    curSes = listOfPoints.get(i).getSessionNumber();
                    LatLng point1 = new LatLng(listOfPoints.get(i - 1).getLatitude(), listOfPoints.get(i - 1).getLongitude());
                    mMap.addPolyline(new PolylineOptions()
                            .color(Color.RED)
                            .add(
                                    point1,
                                    point
                            ));
                }else if(curSes != listOfPoints.get(i).getSessionNumber()){
                    curSes = listOfPoints.get(i).getSessionNumber();
                    tmpi = i;
                }
            }

            GpsPoint tmp = listOfPoints.get(listOfPoints.size() - 1);
            listOfPoints.removeAll(listOfPoints);
            listOfPoints.add(tmp);
        }
    }

}
