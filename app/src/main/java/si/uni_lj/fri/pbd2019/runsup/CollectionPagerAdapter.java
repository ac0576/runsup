package si.uni_lj.fri.pbd2019.runsup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentManager;

import si.uni_lj.fri.pbd2019.runsup.fragments.ActiveWorkoutMapFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.WidgetFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;


public class CollectionPagerAdapter extends FragmentStatePagerAdapter {

    public CollectionPagerAdapter(FragmentManager fm){
        super(fm);
    }
    private String tabTitles[] = new String[]{"Widgets", "Stopwatch", "Map"};

    @Override
    public Fragment getItem(int i) {
        if(i == 0)
            return new WidgetFragment();
        else if(i == 1)
            return new StopwatchFragment();
        else if(i == 2)
            return new ActiveWorkoutMapFragment();
        return new StopwatchFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        return 3;
    }
}
