package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

import androidx.annotation.Nullable;

import si.uni_lj.fri.pbd2019.runsup.R;

public class SettingsFragment extends PreferenceFragment {

    private SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener;

    private EditTextPreference goal, milestone;
    private SwitchPreference goalc, milestonec;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_main);
        goal = (EditTextPreference) findPreference("goal_value");
        goalc = (SwitchPreference) findPreference("goal");
        milestone = (EditTextPreference) findPreference("milestone_value");
        milestonec = (SwitchPreference) findPreference("milestone");

        preferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if(key.equals("gps_switch")){
                    Preference gps = findPreference(key);

                }else if(key.equals("unit")){
                    ListPreference listPreference = (ListPreference) findPreference("unit");
                    Preference list = findPreference(key);
                    CharSequence currText = listPreference.getEntry();
                    String currValue = listPreference.getValue();
                    list.setSummary(currText);
                }else if(key.equals("goal_value")){
                    goal.setTitle(goal.getText() + " m");
                }else if(key.equals("milestone_value")){
                    milestone.setTitle(milestone.getText() + " m");
                }
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(preferenceChangeListener);
        ListPreference listPreference = (ListPreference) findPreference("unit");
        Preference list = findPreference("unit");
        CharSequence currText = listPreference.getEntry();
        list.setSummary(currText);
        goal.setTitle(goal.getText() + " m");
        milestone.setTitle(milestone.getText() + " m");


    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }
}
