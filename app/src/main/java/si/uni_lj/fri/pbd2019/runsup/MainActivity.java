package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentManager;
import si.uni_lj.fri.pbd2019.runsup.fragments.AboutFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.ActiveWorkoutMapFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.CollectionFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.WidgetFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        AboutFragment.OnFragmentInteractionListener,
        StopwatchFragment.OnFragmentInteractionListener,
        HistoryFragment.OnFragmentInteractionListener,
        CollectionFragment.OnFragmentInteractionListener,
        WidgetFragment.OnFragmentInteractionListener,
        ActiveWorkoutMapFragment.OnFragmentInteractionListener {

    private String fragmentTag = "fragment_stopwatch";
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private ImageView loginImg;
    private TextView loginUname;
    private static final String TAG = "MainActivity";
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        View header = navigationView.getHeaderView(0);
        loginImg = (ImageView) header.findViewById(R.id.menu_loggedInUserImage);
        loginUname = (TextView) header.findViewById(R.id.menu_loggedInUserFullName);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        if(savedInstanceState == null) {
            fragmentManager.beginTransaction().add(R.id.fragment_container, new CollectionFragment(), Constants.STOPWATCH_TAG).addToBackStack(Constants.STOPWATCH_TAG).commit();
            navigationView.setCheckedItem(R.id.nav_workout);
        }

        loginImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(login);
            }
        });

        loginUname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(login);
            }
        });
        startParser();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(getSupportFragmentManager().getBackStackEntryCount() == 1)
                moveTaskToBack(true);
            else if(getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
                int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
                String tag = getSupportFragmentManager().getBackStackEntryAt(index).getName();
                getSupportFragmentManager().beginTransaction().show(getSupportFragmentManager().findFragmentByTag(tag)).commit();
                updateCurrentSelectedItemBackground(tag);

                //getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentByTag(fragmentTag)).commit();

                fragmentTag = new String(tag);
            }else
                super.onBackPressed();
        }
    }

    private void updateCurrentSelectedItemBackground(String tag){
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if(tag.equals(Constants.STOPWATCH_TAG))
            navigationView.setCheckedItem(R.id.nav_workout);
        else if(tag.equals(Constants.HISTORY_TAG))
            navigationView.setCheckedItem(R.id.nav_history);
        else if(tag.equals(Constants.SETTINGS_TAG))
            navigationView.setCheckedItem(R.id.nav_settings);
        else if(tag.equals(Constants.ABOUT_TAG))
            navigationView.setCheckedItem(R.id.nav_about);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        boolean loggedin = prefs.getBoolean(getString(R.string.userSignedIn), false);
        if(!loggedin){
            MenuItem item = menu.findItem(R.id.action_sync);
            item.setVisible(false);
        }
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            onNavigationItemSelected(item);
            return true;
        }else if(item.getItemId() == R.id.action_sync){
            syncParser();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_workout && !fragmentTag.equals(Constants.STOPWATCH_TAG)) {

            if(getSupportFragmentManager().findFragmentByTag(Constants.STOPWATCH_TAG) != null) {
                //if the fragment exists, show it.
                getSupportFragmentManager().beginTransaction()
                        .show(getSupportFragmentManager().findFragmentByTag(Constants.STOPWATCH_TAG))
                        .addToBackStack(Constants.STOPWATCH_TAG).commit();
            } else {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container,
                                new CollectionFragment(), Constants.STOPWATCH_TAG)
                        .addToBackStack(Constants.STOPWATCH_TAG).commit();
            }
                //if the other fragment is visible, hide it.
            getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentByTag(fragmentTag)).commit();
            fragmentTag = Constants.STOPWATCH_TAG;
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new StopwatchFragment()).addToBackStack(null).commit();
        } else if (id == R.id.nav_history && !fragmentTag.equals(Constants.HISTORY_TAG)) {

            if(getSupportFragmentManager().findFragmentByTag(Constants.HISTORY_TAG) != null) {
                //if the fragment exists, show it.
                getSupportFragmentManager().beginTransaction().show(getSupportFragmentManager().findFragmentByTag(Constants.HISTORY_TAG)).addToBackStack(Constants.HISTORY_TAG).commit();
            } else {
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new HistoryFragment(), Constants.HISTORY_TAG).addToBackStack(Constants.HISTORY_TAG).commit();
            }
            //if the other fragment is visible, hide it.
            getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentByTag(fragmentTag)).commit();
            fragmentTag = Constants.HISTORY_TAG;

        } else if (id == R.id.nav_settings) {
            Intent in = new Intent(this, SettingsActivity.class);
            startActivity(in);
        } else if (id == R.id.nav_about && !fragmentTag.equals(Constants.ABOUT_TAG)) {
            if(getSupportFragmentManager().findFragmentByTag(Constants.ABOUT_TAG) != null) {
                //if the fragment exists, show it.
                getSupportFragmentManager().beginTransaction().show(getSupportFragmentManager().findFragmentByTag(Constants.ABOUT_TAG)).addToBackStack(Constants.ABOUT_TAG).commit();
            } else {
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new AboutFragment(), Constants.ABOUT_TAG).addToBackStack(Constants.ABOUT_TAG).commit();
            }
            //if the other fragment is visible, hide it.
            getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentByTag(fragmentTag)).commit();
            fragmentTag = Constants.ABOUT_TAG;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onFragmentInteraction(Uri uri){
        //you can leave it empty
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCurrentSelectedItemBackground(fragmentTag);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        boolean loggedin = prefs.getBoolean(getString(R.string.userSignedIn), false);
        //Log.d(TAG, "User is logged = " + loggedin);
        if(menu!=null) {
            MenuItem item = menu.findItem(R.id.action_sync);
            if (!loggedin) {
                item.setVisible(false);
            } else {
                item.setVisible(true);
            }
        }
        changeUser();
    }

    private void changeUser(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String name = prefs.getString(Constants.SHARED_USER, getString(R.string.all_unknownuser));
        String picURL = prefs.getString(Constants.SHARED_USER_PIC, "");

        if(!picURL.equals("") && picURL != null) {
            new DownloadImageTask(loginImg)
                    .execute(picURL);
        }else{
            loginImg.setImageResource(R.mipmap.ic_launcher_round);
        }

        loginUname.setText(name);

    }

    //https://stackoverflow.com/questions/2471935/how-to-load-an-imageview-by-url-in-android
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public void syncParser(){
        List<Workout> workoutsByUser = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        Dao<Workout, Long> workoutLongDao = null;


        try {
            workoutLongDao = databaseHelper.workoutDao();
            QueryBuilder<Workout, Long> builder = workoutLongDao.queryBuilder();
            User tmp = getLoggedUser();
            builder.where().eq("user_id", tmp.getId());
            workoutsByUser = workoutLongDao.query(builder.prepare());  // returns list of ten items
            if(workoutsByUser.size()>0){
                Log.d(TAG, workoutsByUser.get(0).getUser()+ "");
            }
            for(int i=0; i<workoutsByUser.size(); i++) {
                ParseObject workoutList = new ParseObject("Workout");
                workoutList.put("id", workoutsByUser.get(i).getId());
                workoutList.put("user", tmp.getId());
                workoutList.put("title", workoutsByUser.get(i).getTitle());
                workoutList.put("created", workoutsByUser.get(i).getCreated());
                workoutList.put("status", workoutsByUser.get(i).getStatus());
                workoutList.put("distance", workoutsByUser.get(i).getDistance());
                workoutList.put("duration", workoutsByUser.get(i).getDuration());
                workoutList.put("totalCalories", workoutsByUser.get(i).getTotalCalories());
                workoutList.put("paceAvg", workoutsByUser.get(i).getPaceAvg());
                workoutList.put("sportActivity", workoutsByUser.get(i).getSportActivity());
                workoutList.put("lastUpdate", workoutsByUser.get(i).getLastUpdate());
                syncGpsPoints(workoutsByUser.get(i).getId());
                workoutList.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        if (e == null) {
                            Log.d(TAG, "all OK");
                            deleteLocal();

                        } else {
                            Log.d(TAG, "error saving " + e.getMessage().toString());

                            Toast.makeText(
                                    getApplicationContext(),
                                    e.getMessage().toString(),
                                    Toast.LENGTH_LONG
                            ).show();
                        }
                    }
                });
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteLocal(){
        Dao<Workout, Long> workoutLongDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        try {
            workoutLongDao = databaseHelper.workoutDao();

            DeleteBuilder<Workout, Long> deleteBuilder = workoutLongDao.deleteBuilder();
            workoutLongDao.delete(deleteBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User getLoggedUser(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        //String ACCID = prefs.getString(Constants.SHARED_USER_UID, "77777");
        String ACCID = prefs.getString(getString(R.string.userId), "");

        List<User> users = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        Dao<User, Long> userLongDao = null;


        try {
            userLongDao = databaseHelper.userDao();
            QueryBuilder<User, Long> builder = userLongDao.queryBuilder();
            builder.where().eq("accId", ACCID);
            users = userLongDao.query(builder.prepare());  // returns list of ten items

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        if(users.size() > 0) {

            return users.get(0);
        }else
            return null;
    }

    public void startParser(){
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.back4app_app_id))
                // if defined
                .clientKey(getString(R.string.back4app_client_key))
                .server(getString(R.string.back4app_server_url))
                .build()
        );

        Log.d(TAG, "Parser started");
    }

    public void syncGpsPoints(Long workout_id){
        List<GpsPoint> gpsList = new ArrayList<GpsPoint>();
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        Dao<GpsPoint, Long> gpsPointLongDao = null;


        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            QueryBuilder<GpsPoint, Long> builder = gpsPointLongDao.queryBuilder();
            User tmp = getLoggedUser();
            builder.where().eq("workout_id", workout_id);
            gpsList = gpsPointLongDao.query(builder.prepare());  // returns list of ten items

            for(int i=0; i<gpsList.size(); i++) {
                ParseObject gpsPoint = new ParseObject("GpsPoint");
                gpsPoint.put("id", gpsList.get(i).getId());
                gpsPoint.put("workout_id", workout_id);
                gpsPoint.put("sessionNumber", gpsList.get(i).getSessionNumber());
                gpsPoint.put("latitude", gpsList.get(i).getLatitude());
                gpsPoint.put("longitude", gpsList.get(i).getLongitude());
                gpsPoint.put("duration", gpsList.get(i).getDuration());
                gpsPoint.put("speed", gpsList.get(i).getSpeed());
                gpsPoint.put("pace", gpsList.get(i).getPace());
                gpsPoint.put("totalCalories", gpsList.get(i).getTotalCalories());
                gpsPoint.put("created", gpsList.get(i).getCreated());
                gpsPoint.put("lastUpdate", gpsList.get(i).getLastUpdate());
                gpsPoint.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        if (e == null) {
                            Log.d(TAG, "Gps points saved.");
                            deleteLocalGPSp();

                        } else {
                            Log.d(TAG, "error saving GPS points: " + e.getMessage().toString());

                            Toast.makeText(
                                    getApplicationContext(),
                                    e.getMessage().toString(),
                                    Toast.LENGTH_LONG
                            ).show();
                        }
                    }
                });
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteLocalGPSp(){
        Dao<GpsPoint, Long> gpsPointLongDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            DeleteBuilder<GpsPoint, Long> deleteBuilder = gpsPointLongDao.deleteBuilder();
            gpsPointLongDao.delete(deleteBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "Req code is " + requestCode);
        if(requestCode == 777){
            Intent trackerIntent  = new Intent(this, TrackerService.class);
            startService(trackerIntent);
        }
    }
}
