package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.util.Log;

import java.util.Hashtable;
import java.util.List;

public final class SportActivities {

    public static final int RUNNING = 0;
    public static final int WALKING = 1;
    public static final int CYCLING = 2;

    private static final Hashtable<Integer,Double> Running = new Hashtable<Integer,Double>()
    {{
        put(4, 6.0);
        put(5, 8.3);
        put(6, 9.8);
        put(7, 11.0);
        put(8, 11.8);
        put(9, 12.8);
        put(10, 14.5);
        put(11, 16.0);
        put(12, 19.0);
        put(13, 19.8);
        put(14, 23.0);

    }};

    private static final Hashtable<Integer,Double> Walking = new Hashtable<Integer,Double>()
    {{
        put(1, 2.0);
        put(2, 2.8);
        put(3, 3.1);
        put(4, 3.5);
    }};

    private static final Hashtable<Integer,Double> Cycling = new Hashtable<Integer,Double>()
    {{
        put(10, 6.8);
        put(12, 8.0);
        put(14, 10.0);
        put(16, 12.8);
        put(18, 13.6);
        put(20, 15.8);
    }};



    /**
     * Returns MET value for an activity.
     * @param activityType - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param speed - speed in m/s
     * @return
     */
    public static double getMET(int activityType, double speed){
        speed =  MainHelper.mpsToMiph(speed);
        int sp = (int) Math.ceil(speed);
        switch (activityType) {
            case 0:
                if(sp > 3 && sp < 15)
                    return Running.get(sp);
                else
                    return speed*1.535353535;
            case 1:
                if(sp<5)
                    return Walking.get(sp);
                else
                    return speed*1.14;
            case 2:
                if(sp>9 && sp < 21)
                    return Cycling.get(sp);
                else
                    return speed*0.744444444;
            default:
                break;
        }
        return 0;
    }

    /**
     * Returns final calories computed from the data provided (returns value in kcal)
     * @param sportActivity - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param weight - weight in kg
     * @param speedList - list of all speed values recorded (unit = m/s)
     * @param timeFillingSpeedListInHours - time of collecting speed list (duration of sport activity from first to last speedPoint in speedList)
     * @return
     */
    public static double countCalories(int sportActivity, float weight, List<Float> speedList, double timeFillingSpeedListInHours){
        double avgSpeed = 0;
        for (int i = 0;  i<speedList.size(); i++){
            avgSpeed += speedList.get(i);
        }
        avgSpeed =  avgSpeed / speedList.size();
        double MET = getMET(sportActivity, avgSpeed);
        //Log.d("SportActivities", "MET: " + MET + " weight: " + weight + " time: " + timeFillingSpeedListInHours);
        return MET*weight*timeFillingSpeedListInHours;
    }

}
