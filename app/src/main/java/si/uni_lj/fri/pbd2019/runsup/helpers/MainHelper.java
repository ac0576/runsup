package si.uni_lj.fri.pbd2019.runsup.helpers;

import java.util.Hashtable;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class MainHelper {

    private static final float MpS_TO_MIpH = 2.23694f;
    private static final float KM_TO_MI = 0.62137119223734f;
    private static final float MINpKM_TO_MINpMI = 1.609344f;

    public static final Hashtable<Integer,String> activity = new Hashtable<Integer,String>()
    {{
        put(0, "Running");
        put(1, "Walking");
        put(2, "Cycling");
    }};

    public static String activityToString(int a){
        return activity.get(a);
    }

    /**
     * return string of time in format HH:MM:SS
     * @param time - in seconds
     */
    public static String formatDuration(long time){
        return String.format(Locale.ENGLISH,"%02d:%02d:%02d", TimeUnit.SECONDS.toHours(time),
                TimeUnit.SECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(time)),
                time - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(time)));
    }

    /**
     * convert m to km and round to 2 decimal places and return as string
     */
    public static String formatDistance(double n){
        return Double.toString(Math.round(n /10) / 100.0);
    }

    /**
     * round number to 2 decimal places and return as string
     */
    public static String formatPace(double n){
        return String.format ("%.2f", n);
    }

    /**
     * round number to integer
     */
    public static String formatCalories(double n){
        return Integer.toString((int) Math.round(n));
    }

    /* convert km to mi (multiply with a corresponding constant) */
    public static double kmToMi(double n){
        return n*KM_TO_MI;
    }

    /* convert m/s to mi/h (multiply with a corresponding constant) */
    public static double mpsToMiph(double n){
        return n*MpS_TO_MIpH;
    }

    /* convert min/km to min/mi (multiply with a corresponding constant) */
    public static double minpkmToMinpmi(double n){
        return n*MINpKM_TO_MINpMI;
    }

    public static double msToMin (long n){
        return n/60000;
    }

}
