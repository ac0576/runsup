package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;

@DatabaseTable
public class User {

    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField
    private String accId;

    @DatabaseField
    private String authToken;

    public User(){}

    public User(String accId){
        this.accId = accId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
