package si.uni_lj.fri.pbd2019.runsup.settings;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.fragments.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if(findViewById(R.id.fragment_container) != null)
            if(savedInstanceState != null)
                return;
            else
                getFragmentManager().beginTransaction().add(R.id.fragment_container, new SettingsFragment()).commit();
    }
}
