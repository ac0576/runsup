package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StopwatchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StopwatchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StopwatchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Button start;
    private Button end;
    private int pressed = -1;
    private TextView duration;
    private TextView distance;
    private TextView pace;
    private TextView calories;
    private Button selectSport;
    private int activity = 0;
    private long durationValue = 0;
    private double distanceValue = 0;
    private double paceValue = 0;
    private double caloriesValue = 0;
    private boolean serviceIsDead = true;
    //private Button showMap;
    private long workoutId = -1;
    private static final String TAG = "StopwatchFragment";

    private TextView distanceLabel, paceLabel;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private boolean kmLabel = true;

    private OnFragmentInteractionListener mListener;
    BroadcastReceiver mBroadcastReceiver;

    public StopwatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StopwatchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StopwatchFragment newInstance(String param1, String param2) {
        StopwatchFragment fragment = new StopwatchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        start = (Button) v.findViewById(R.id.button_stopwatch_start);
        start.setText(R.string.stopwatch_start);
        end = (Button) v.findViewById(R.id.button_stopwatch_endworkout);
        //showMap = (Button) v.findViewById(R.id.button_stopwatch_activeworkout);
        end.setVisibility(View.GONE);
        duration = (TextView) v.findViewById(R.id.textview_stopwatch_duration);
        duration.setText("00:00:00");
        distance = (TextView) v.findViewById(R.id.textview_stopwatch_distance);
        distance.setText("0");
        pace = (TextView) v.findViewById(R.id.textview_stopwatch_pace);
        pace.setText("0.0");
        calories = (TextView) v.findViewById(R.id.textview_stopwatch_calories);
        calories.setText("0");
        selectSport = (Button) v.findViewById(R.id.button_stopwatch_selectsport);

        distanceLabel = (TextView) v.findViewById(R.id.textview_stopwatch_distanceunit);
        paceLabel = (TextView) v.findViewById(R.id.textview_stopwatch_unitpace);



        if(loadSavedPreferences()){
            setSavedData();
            savePreferences(false);
        }

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent trackerIntent  = new Intent(getActivity(), TrackerService.class);
                trackerIntent.putExtra("activity", activity);
                if(pressed == -1 ) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                    if(sharedPreferences.getBoolean("gps_switch", true)) {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // we need to show the explanation to the user, so we show the Snackbar
                            Snackbar.make(getActivity().findViewById(R.id.drawer_layout), R.string.permission_location_rationale,
                                    Snackbar.LENGTH_INDEFINITE)
                                    .setAction(R.string.ok, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            // If the user agrees with the Snackbar, proceed with asking for the permissions:
                                            ActivityCompat.requestPermissions(getActivity(),
                                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                                            Manifest.permission.ACCESS_FINE_LOCATION},
                                                    777);
                                        }
                                    }).show();

                        }
                    }
                    start.setText(R.string.stopwatch_stop);
                    end.setVisibility(View.GONE);

                    trackerIntent.setAction(Constants.START);
                    getActivity().startService(trackerIntent);
                    serviceIsDead = false;
                    pressed = 1;
                }else if (pressed == 0){
                    start.setText(R.string.stopwatch_continue);
                    end.setVisibility(View.VISIBLE);
                    trackerIntent.setAction(Constants.PAUSE);
                    getActivity().startService(trackerIntent);
                    serviceIsDead = false;

                    //finalPositionList.add(tmp);
                    //tmp = new ArrayList<>();

                }else  if(pressed == 1 ){
                    start.setText(R.string.stopwatch_stop);
                    end.setVisibility(View.GONE);
                    trackerIntent.setAction(Constants.CONTINUE);
                    getActivity().startService(trackerIntent);
                    serviceIsDead = false;
                }

                pressed=1-pressed;
            }
        });
        selectSport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.selectsport_title);
                builder.setItems(R.array.selectsport_activities, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity = which;
                        Log.d(TAG, "Activity set to " + activity);
                        Resources res = getResources();
                        String[] planets = res.getStringArray(R.array.selectsport_activities);
                        selectSport.setText(planets[which]);
                        Intent selectSport = new Intent(getActivity(), TrackerService.class);
                        selectSport.setAction(Constants.SELECT_SPOT);
                        selectSport.putExtra("ID", which);
                        getActivity().startService(selectSport);

                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                builder.show();
            }
        });

        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finalPositionList.add(tmp);
                //tmp = new ArrayList<>();
                new AlertDialog.Builder(getActivity())
                        .setMessage(R.string.stopwatch_end_message)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int which) {
                                Intent workoutResults = new Intent(getActivity(), WorkoutDetailActivity.class);
                                Intent trackerIntent  = new Intent(getActivity(), TrackerService.class);
                                trackerIntent.setAction(Constants.STOP);
                                getActivity().startService(trackerIntent);
                                serviceIsDead = true;

                                workoutResults.putExtra("sportActivity", activity);
                                workoutResults.putExtra("duration", durationValue);
                                workoutResults.putExtra("distance", distanceValue);
                                workoutResults.putExtra("pace", paceValue);
                                workoutResults.putExtra("calories", caloriesValue);
                               // workoutResults.putExtra("finalPositionList", finalPositionList);
                                workoutResults.putExtra("workoutId", workoutId);
                                Log.d(TAG, "Workout id in SF is: " + workoutId);
                                pressed = -1;
                                //put everything here
                                getActivity().startActivity(workoutResults);
                                savePreferences(false);
                                //Fragment currentFragment = getFragmentManager().findFragmentByTag(Constants.STOPWATCH_TAG);
                                List<Fragment> fragmentListi = getFragmentManager().getFragments();
                                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                for(int i=0; i<fragmentListi.size(); i++){
                                    Fragment currentFragment = fragmentListi.get(i);
                                    fragmentTransaction.detach(currentFragment);
                                    fragmentTransaction.attach(currentFragment);

                                }
                                fragmentTransaction.commit();


                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .show();

            }
        });
        /*showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activeMap = new Intent(getActivity(), ActiveWorkoutMapActivity.class);
                activeMap.putExtra("workoutId", workoutId);
                startActivity(activeMap);
            }
        });
        */
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            IntentFilter filter = new IntentFilter();
            filter.addAction(Constants.TICK);
            filter.addAction(Constants.DEAD);
            mBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(intent.getAction().equals(Constants.DEAD)){
                        serviceIsDead = true;
                    }else {
                        durationValue = intent.getIntExtra("duration", -1);
                        duration.setText(MainHelper.formatDuration(durationValue));
                        distanceValue = intent.getDoubleExtra("distance", -1);
                        paceValue = intent.getDoubleExtra("pace", 0.0);
                        if(kmLabel) {
                            distance.setText(MainHelper.formatDistance(distanceValue));
                            pace.setText(MainHelper.formatPace(paceValue));
                        }else {
                            distance.setText(MainHelper.formatDistance(MainHelper.kmToMi(distanceValue)));
                            pace.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(paceValue)));
                        }
                        //Log.d("StopwatchActivity", "Value: " + paceValue);
                        caloriesValue = intent.getDoubleExtra("calories", -1);
                        calories.setText(Double.toString(caloriesValue));
                        workoutId = intent.getLongExtra("workoutId", -1);

                        //tmp = intent.getParcelableArrayListExtra("location");
                    }
                }
            };
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver, filter);
            Intent trackerIntent = new Intent(getActivity(), TrackerService.class);
            getActivity().startService(trackerIntent);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //savePreferences(true);
        Intent trackerIntent  = new Intent(getActivity(), TrackerService.class);
        if(!serviceIsDead)
            Log.d("StopwatchActivity123", "Destroyed");
        else
            getActivity().stopService(trackerIntent);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        savePreferences(true);
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        selectSport.setText(R.string.stopwatch_activity);
        activity = 0;
        savePreferences(false);
        kmLabel = loadSavedPreferencesLable();
        if(kmLabel){
            distanceLabel.setText(getString(R.string.stopwatch_unitdistance));
            paceLabel.setText(getString(R.string.stopwatch_unitpace));
        }else{
            distanceLabel.setText(getString(R.string.stopwatch_unitdistance_mi));
            paceLabel.setText(getString(R.string.stopwatch_unitpace_mi));
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public void savePreferences(boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        prefs.edit().putBoolean(Constants.SHARED_USER_CRASH, value).apply();
    }

    public boolean loadSavedPreferences() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean didUserLeft = prefs.getBoolean(Constants.SHARED_USER_CRASH, true);
        return didUserLeft;
    }

    public boolean loadSavedPreferencesLable() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        String unit = sp.getString("unit", "0");
        //Log.d(TAG, "Unit :" + unit);
        if(unit.equals("0"))
            return true;
        else
            return false;
    }

    public void setSavedData(){
        Workout curWorkout = getLastWorkout();

        if(curWorkout != null){
            duration.setText(MainHelper.formatDuration(curWorkout.getDuration()));
            distance.setText(MainHelper.formatDistance(curWorkout.getDistance()));
            pace.setText("0.0");
            calories.setText(Double.toString(curWorkout.getTotalCalories()));
            workoutId = curWorkout.getId();


            Intent trackerIntent  = new Intent(getActivity(), TrackerService.class);
            trackerIntent.setAction(Constants.PAUSE);
            trackerIntent.putExtra("restore" , true);
            getActivity().startService(trackerIntent);
            serviceIsDead = false;
            start.setText(R.string.stopwatch_continue);
            end.setVisibility(View.VISIBLE);
            //tmp = new ArrayList<>();
        }
    }

    public Workout getLastWorkout(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(),
                DatabaseHelper.class);
        Dao<Workout, Long> workoutLongDao = null;
        try {
            workoutLongDao = databaseHelper.workoutDao();
            //gpsPointDao.create(curGpsPoint)
            QueryBuilder<Workout, Long> builder = workoutLongDao.queryBuilder();
            builder.orderBy("created", false);
            List<Workout> list = workoutLongDao.query(builder.prepare());  // returns list of ten items
            Log.d(TAG, "Got workouts " + list.size());
            if(list.size()>0)
                return list.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }



}
