package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.MainActivity;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class TrackerService extends Service {

    private static final String TAG = "TrackerService";

    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;
    private ArrayList<Location> locations;
    private long elapsedTime = 0;
    private int milestones_reached = 0;
    //in sec
    private boolean done = true;
    private final int FREQ_UDATE = 3000;
    //in meters
    private final int DISTANCE_UPDATE = 10;
    private int state;
    private double totalDistance = 0.0;
    private double lastDistance = 0.0;
    private int lastPeriodBetweenPoints = 0;
    private Location lastPoint;
    private int activity = 0;
    private long timeElapsed = 0;
    private long totalTime = 0;
    private long tempTime = 0;
    private long lastTimeBetweenPoints = 0;
    private long newTimeBetweenPoints = 0;
    private long setPaceTime = 0;
    private double oldPace = 0;
    private ArrayList<Float> speedlist = new ArrayList<>();
    private long speedTimeBig = 0;
    private long speedTimeEnd = 0;
    private DatabaseHelper databaseHelper = null;
    private Workout curWorkout;
    private GpsPoint curGpsPoint;
    private int sessonNum = 0;
    private int nTicks = 0;
    private double totalPace = 0;
    private int userHeight, userWeight, userAge;
    private SharedPreferences mSharedPreferences;
    private Location tmpL;


    Handler mHandler = new Handler();
    Runnable rBroadcast = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(rBroadcast, 1000);
            prepareBroadcast();
        }
    };

    private final IBinder mBinder = new LocalBinder();

    LocationCallback mLocationCallback = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location location = locationResult.getLastLocation();
            if(location != null) {
                lastTimeBetweenPoints = newTimeBetweenPoints;
                newTimeBetweenPoints = System.currentTimeMillis();

                if(lastPoint != null && lastPoint.distanceTo(location) >= 2){
                    lastDistance = lastPoint.distanceTo(location);

                    if(lastDistance > 100){
                        lastDistance = 0;
                        totalDistance = 0;
                        setPaceTime = 0;
                    }else{
                        int seconds = Math.round((newTimeBetweenPoints - lastTimeBetweenPoints) / 1000);
                        double distance = lastPoint.distanceTo(location);
                        if(speedlist.size() == 0)
                            speedTimeBig = System.currentTimeMillis();
                        speedlist.add((float) distance/seconds);
                        speedTimeEnd = System.currentTimeMillis();
                        setPaceTime = tempTime;
                    }
                    totalDistance += lastDistance;

                }

                if(lastDistance >= 2 || locations.size() == 0) {

                    lastPeriodBetweenPoints = 0;
                    Log.d(TAG, location.getLongitude() + " " + location.getLatitude());
                    locations.add(location);
                    lastPoint = locationResult.getLastLocation();
                }
                Log.d(TAG,  "Lat: " + location.getLatitude() +" long:" + location.getLongitude());
            }
        }
    };


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public TrackerService getService() {
            return TrackerService.this;
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(FREQ_UDATE);
        locationRequest.setFastestInterval(FREQ_UDATE);
        locationRequest.setSmallestDisplacement(DISTANCE_UPDATE);
        locations = new ArrayList<Location>();

        databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            Dao<Workout, Long> workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        curWorkout = new Workout();
        getSharedData();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null) {
            boolean restore = intent.getBooleanExtra("restore", false);
            getLocations();
            if (intent.getAction() != null) {
                if (intent.getAction().equals(Constants.START)) {
                    done = true;
                    locations = new ArrayList<Location>();
                    state = Constants.STATE_RUNNING;
                    sessonNum++;
                    Tick();
                } else if (intent.getAction().equals(Constants.PAUSE)) {
                    if(restore){
                        getAndSetLast();
                    }
                    sessonNum++;
                    totalTime = tempTime + 1;
                    state = Constants.STATE_PAUSED;
                    mHandler.removeCallbacks(rBroadcast);
                    if (fusedLocationClient != null) {
                        fusedLocationClient.removeLocationUpdates(mLocationCallback);
                    }
                    locations = new ArrayList<Location>();
                } else if (intent.getAction().equals(Constants.CONTINUE)) {
                    timeElapsed = System.currentTimeMillis();
                    state = Constants.STATE_CONTINUE;
                    getLocations();
                    Tick();
                } else if (intent.getAction().equals(Constants.STOP)) {
                    finishWorkout();
                    state = Constants.STATE_STOPPED;
                    if (fusedLocationClient != null) {
                        fusedLocationClient.removeLocationUpdates(mLocationCallback);
                    }
                    mHandler.removeCallbacks(rBroadcast);
                } else if (intent.getAction().equals(Constants.SELECT_SPOT)) {
                    activity = intent.getIntExtra("ID", 0);
                    curWorkout.setSportActivity(activity);
                    Log.d(TAG, "Activity set to " + activity);
                    //updateWokrout();

                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (fusedLocationClient != null) {
            fusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        mHandler.removeCallbacks(rBroadcast);
        Intent dead = new Intent(Constants.DEAD);
        LocalBroadcastManager.getInstance(this).sendBroadcast(dead);
    }

    public void Tick(){
        timeElapsed = System.currentTimeMillis();
        mHandler.post(rBroadcast);
    }

    public void getLocations() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this, "Please allow us to track you so we can show you your location.", Toast.LENGTH_SHORT).show();


        }

        if (fusedLocationClient != null) {
            fusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        Log.d(TAG, "Restarting locations.");
        fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());

    }

    public void prepareBroadcast(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean milestone = sharedPreferences.getBoolean("milestone", false);
        boolean goal = sharedPreferences.getBoolean("goal", false);
        double distd = getDistance();
        int dist = (int) distd;
        if(milestone){
            int milestone_val = Integer.parseInt(sharedPreferences.getString("milestone_value", "0"));

            if(milestone_val > 0){
                int con = dist/milestone_val;
                //Log.d(TAG, "milestone rec: " + con + " dist " + dist+ " raw " + distd + " mval "+ milestone_val);

                if(con > milestones_reached){
                    Log.d(TAG, "ding");
                    milestones_reached=con;
                    MediaPlayer dingSound = MediaPlayer.create(this, R.raw.ding);
                    dingSound.start();
                }
            }
        }
        if(goal){
            int goal_val = Integer.parseInt(sharedPreferences.getString("goal_value", "0"));
            if(goal_val > 0 && dist>=goal_val && done){
                done = false;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    CharSequence name = "not_channel";
                    String description = "not_channel";
                    int importance = NotificationManager.IMPORTANCE_DEFAULT;
                    NotificationChannel channel = new NotificationChannel("notchan", name, importance);
                    channel.setDescription(description);
                    // Register the channel with the system; you can't change the importance
                    // or other notification behaviors after this
                    NotificationManager notificationManager = getSystemService(NotificationManager.class);
                    notificationManager.createNotificationChannel(channel);
                }
                Log.d(TAG,"Notification");
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "notchan")
                        .setSmallIcon(R.drawable.ic_running_man)
                        .setContentTitle("RunsUp")
                        .setContentText("Congratulations you reached your goal")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                        .setLights(Color.GREEN, 3000, 3000)
                        .setAutoCancel(true);

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
                notificationManager.notify(123, builder.build());
            }
        }
        Intent data = new Intent(Constants.TICK);
        int time  = (int) getDuration();
        //Log.d(TAG, "time: " + time);
        lastPeriodBetweenPoints++;
        data.putExtra("duration", time);
        data.putExtra("distance", getDistance());
        double pace = getPace();
        data.putExtra("pace", pace);
        data.putExtra("calories", getCalories());
        data.putExtra("state", getState());
        data.putExtra("sportActivity", activity);
        data.putExtra("workoutId", curWorkout.getId());


        if(locations.size() != 0) {
            data.putExtra("location", locations.get(locations.size() - 1));
            data.putExtra("lat", locations.get(locations.size()-1).getLatitude());
            data.putExtra("lng", locations.get(locations.size()-1).getLongitude());
        }
        if(nTicks == 0)
            createWokrout();
        else
            updateWokrout();

        if(locations.size() != 0) {
            if(tmpL == null) {
                putGpsPoint(time, pace);
            }else if(tmpL != locations.get(locations.size()-1)) {
                putGpsPoint(time, pace);
            }
        }

        nTicks++;

        //Do workout
        //...


        LocalBroadcastManager.getInstance(this).sendBroadcast(data);
        //locations = new ArrayList<Location>();
    }

    public void putGpsPoint(int time, double pace){
        Log.d(TAG, "locations list size " + locations.size());
        tmpL = locations.get(locations.size()-1);
        curGpsPoint = new GpsPoint();
        curGpsPoint.setCreated(new Date());
        curGpsPoint.setDuration(time);
        curGpsPoint.setLatitude(locations.get(locations.size()-1).getLatitude());
        curGpsPoint.setLongitude(locations.get(locations.size()-1).getLongitude());
        curGpsPoint.setPace(pace);
        curGpsPoint.setTotalCalories(getCalories());
        curGpsPoint.setWorkout(curWorkout);
        curGpsPoint.setSessionNumber(sessonNum);
        curGpsPoint.setSpeed(locations.get(locations.size()-1).getSpeed());
        curGpsPoint.setLastUpdate(new Date());

        Dao<GpsPoint, Long> gpsPointDao = null;
        try {
            gpsPointDao = databaseHelper.gpsPointDao();
            gpsPointDao.create(curGpsPoint);
            //gpsPointDao.create(curGpsPoint);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        /*QueryBuilder<GpsPoint, Long> builder = gpsPointDao.queryBuilder();
        builder.orderBy("duration", true);  // true for ascending, false for descending
        try {
            List<GpsPoint> list = gpsPointDao.query(builder.prepare());  // returns list of ten items
            for(int i = 0; i < list.size(); i++){
                Log.d(TAG, list.get(i).getDuration() + "");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        */


        curGpsPoint = null;

    }

    public int getState(){
        return state;
    }
	public long getDuration(){
        long cur = System.currentTimeMillis();
        tempTime =  cur - timeElapsed + totalTime;
        return tempTime/1000;
    }
    public double getDistance(){

        return totalDistance;
    }
	public double getPace(){
        long timeDiff = newTimeBetweenPoints - lastTimeBetweenPoints;
        if((lastTimeBetweenPoints !=0 && (newTimeBetweenPoints - lastTimeBetweenPoints) > 2 * FREQ_UDATE)
                || lastPoint == null || locations.size() < 2 || tempTime - setPaceTime > 2 * FREQ_UDATE){

            return 0.0;
        }

        //Log.d(TAG, "Distance: " + timeDiff + " Time: " + MainHelper.msToMin(timeDiff));
        double res  = timeDiff / (lastPoint.distanceTo(locations.get(locations.size()-2)));
        if(oldPace != res ) {
            oldPace = res;
            setPaceTime = tempTime;
        }

        return (res*1000)/60000;
    }

    public double getSpeed(){
        long timeDiff = newTimeBetweenPoints - lastTimeBetweenPoints;
        if((lastTimeBetweenPoints !=0 && (newTimeBetweenPoints - lastTimeBetweenPoints) > 2 * FREQ_UDATE)
                || lastPoint == null || locations.size() < 2 || tempTime - setPaceTime > 2 * FREQ_UDATE){

            return 0.0;
        }

        double res  =  (lastPoint.distanceTo(locations.get(locations.size()-2)))/timeDiff;

        return (res*60000)/1000;
    }

    public double getCalories(){
        double speedTimeInHours = (speedTimeEnd - speedTimeBig)/3600000;

        if(speedlist.size()>2){
            int defaultWeight = userWeight;
            return SportActivities.countCalories(activity, defaultWeight , speedlist, speedTimeInHours);
        }
        return 0;
    }

    public void updateWokrout(){
        Dao<Workout, Long> workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        curWorkout.setDistance(getDistance());
        curWorkout.setDuration(getDuration());
        curWorkout.setTotalCalories(getCalories());
        totalPace += getPace();
        curWorkout.setPaceAvg(totalPace / (nTicks+1));
        try {
            workoutDao = databaseHelper.workoutDao();
            workoutDao.update(curWorkout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createWokrout(){

        String userId = mSharedPreferences.getString(getString(R.string.userId), "");
        List<User> user = null;
        Dao<User, Long> userLongDao = null;
        try {
            userLongDao = databaseHelper.userDao();
            QueryBuilder<User, Long> builder = userLongDao.queryBuilder();
            builder.where().eq("accid", userId);
            user = userLongDao.query(builder.prepare());  // returns list of ten items
        } catch (SQLException e) {
            e.printStackTrace();
        }


        Dao<Workout, Long> workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        curWorkout.setCreated(new Date());
        curWorkout.setLastUpdate(new Date());
        curWorkout.setSportActivity(activity);
        curWorkout.setDistance(getDistance());
        curWorkout.setDuration(getDuration());
        curWorkout.setTotalCalories(getCalories());
        curWorkout.setPaceAvg(getPace());
        curWorkout.setFinished(false);
        totalPace += getPace();
        if(user.size() > 0) {
            Log.d(TAG, user.get(0).getId()+"");
            curWorkout.setUser(user.get(0));
        }
        try {
            workoutDao = databaseHelper.workoutDao();
            workoutDao.create(curWorkout);
            curWorkout.setTitle("Workout " + curWorkout.getId());
            Log.d(TAG, "Cur workout id is: " + curWorkout.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getSharedData(){
        userAge = mSharedPreferences.getInt(Constants.SHARED_USER_AGE, 25);
        userWeight = mSharedPreferences.getInt(Constants.SHARED_USER_WEIGHT, 80);
        userHeight = mSharedPreferences.getInt(Constants.SHARED_USER_HEIGHT, 180);

        //TODO: SET CUSTOM VALUE AFTER TESTS
    }

    public void getAndSetLast(){
        Workout lastWork = getLastWorkout();

        if(lastWork != null){
            Log.d(TAG, "Duration of workouts: " + lastWork.getDuration());
            totalDistance = lastWork.getDistance();
            tempTime = lastWork.getDuration()*1000;
            curWorkout = lastWork;
        }
    }

    public Workout getLastWorkout(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        Dao<Workout, Long> workoutLongDao = null;
        try {
            workoutLongDao = databaseHelper.workoutDao();
            //gpsPointDao.create(curGpsPoint)
            QueryBuilder<Workout, Long> builder = workoutLongDao.queryBuilder();
            builder.orderBy("created", false);
            List<Workout> list = workoutLongDao.query(builder.prepare());  // returns list of ten items
            Log.d(TAG, "Size of workouts: " + list.size());
            if(list.size()>0)
                return list.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void finishWorkout(){
        curWorkout.setFinished(true);
        Dao<Workout, Long> workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
            workoutDao.update(curWorkout);
            Log.d(TAG, "Finished.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



}
