package si.uni_lj.fri.pbd2019.runsup;

import android.graphics.Color;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private long workoutId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        workoutId = getIntent().getLongExtra("workoutId", -1);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        mMap = googleMap;
        long curSes = 0;
        int tmpi = 0;

        List<GpsPoint> list = getLastPoint();
        if(list.size()>1) {
            for (int i = 0; i < list.size(); i++) {
                if(i == 0 ){
                    curSes = list.get(i).getSessionNumber();
                    tmpi = 0;
                }
                LatLng point = new LatLng(list.get(i).getLatitude(), list.get(i).getLongitude());
                builder.include(point);
                mMap.addMarker(new MarkerOptions().position(point));

                if (i > 0 && curSes == list.get(i).getSessionNumber() && tmpi != i) {
                    curSes = list.get(i).getSessionNumber();
                    LatLng point1 = new LatLng(list.get(i - 1).getLatitude(), list.get(i - 1).getLongitude());
                    mMap.addPolyline(new PolylineOptions()
                            .color(Color.RED)
                            .add(
                                point1,
                                point
                            ));
                }else if(curSes != list.get(i).getSessionNumber()){
                    curSes = list.get(i).getSessionNumber();
                    tmpi = i;
                }
            }
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 20);
            googleMap.moveCamera(cu);
        }

    }

    public List<GpsPoint> getLastPoint(){
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this,
                DatabaseHelper.class);
        Dao<GpsPoint, Long> gpsPointLongDao = null;
        try {
            gpsPointLongDao = databaseHelper.gpsPointDao();
            QueryBuilder<GpsPoint, Long> builder = gpsPointLongDao.queryBuilder();
            builder.where().eq("workout_id", workoutId);
            builder.orderBy("created", true);  // true for ascending, false for descending
            List<GpsPoint> list = gpsPointLongDao.query(builder.prepare());  // returns list of ten items
            if(list.size() > 1)
                return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}