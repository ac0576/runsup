package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.GetWeather;
import si.uni_lj.fri.pbd2019.runsup.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WidgetFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WidgetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WidgetFragment extends Fragment implements GetWeather.TaskListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "WidgetFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button play, next, prev;
    private TextView title, artist, album;

    private boolean wether = false;

    private int toggle = 0;

    private AudioManager mAudioManager;

    private SharedPreferences mSharedPreferences;

    private ImageView icon, cels;
    private TextView temp, tempmax, tempmaxv, tempmin, tempminv, pressure, pressurev,
            humidity, humidityv, starttxt, city, wind, windv;

    private boolean first = true;
    private BroadcastReceiver mReceiver  = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Log.d("Music", "Key: " + key);

                    Object value = bundle.get(key);
                    if (value != null)
                        Log.d("Music", String.format("%s %s (%s)", key,
                                value.toString(), value.getClass().getName()));
                }
            }

            String action = intent.getAction();
            String cmd = intent.getStringExtra("command");
            Log.d("Music", action + " / " + cmd);
            String artistG = intent.getStringExtra("artist");
            String albumG = intent.getStringExtra("album");
            String trackG = intent.getStringExtra("track");
            Log.d("Music", artistG + ":" + albumG + ":" + trackG);

            if (artistG != null) {
                artist.setText(artistG);
                mSharedPreferences.edit().putString(Constants.MUSIC_ARTIST, artistG).apply();
            }
            if (album != null) {
                album.setText(albumG);
                mSharedPreferences.edit().putString(Constants.MUSIC_ALBUM, artistG).apply();
            }
            if (trackG != null) {
                title.setText(trackG);
                mSharedPreferences.edit().putString(Constants.MUSIC_SONG, artistG).apply();
            }
        }



    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!wether && intent.getDoubleExtra("lat", -1)!=-1){
                wether = true;
                LatLng cord = new LatLng(intent.getDoubleExtra("lat", 0),
                        intent.getDoubleExtra("lng", 0));
                new GetWeather(WidgetFragment.this).execute(cord);
            }
        }
    };

    private OnFragmentInteractionListener mListener;

    public WidgetFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WidgetFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WidgetFragment newInstance(String param1, String param2) {
        WidgetFragment fragment = new WidgetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_music, container, false);

        play = view.findViewById(R.id.music_state);
        prev = view.findViewById(R.id.music_prev);
        next = view.findViewById(R.id.music_next);

        title = view.findViewById(R.id.music_title);
        album = view.findViewById(R.id.music_album);
        artist = view.findViewById(R.id.music_artist);

        icon = view.findViewById(R.id.weathe_icon);
        cels = view.findViewById(R.id.weather_celcuis);
        temp = view.findViewById(R.id.weather_tmp);
        tempmax = view.findViewById(R.id.wi_max_temp);
        tempmaxv = view.findViewById(R.id.max_value);
        tempmin = view.findViewById(R.id.wi_min_temp);
        tempminv = view.findViewById(R.id.min_value);
        pressure = view.findViewById(R.id.wi_pressure);
        pressurev = view.findViewById(R.id.pressure_value);
        humidity = view.findViewById(R.id.wi_humidity);
        humidityv  = view.findViewById(R.id.humidity_value);
        starttxt = view.findViewById(R.id.noCord);
        city = view.findViewById(R.id.wi_city);
        windv = view.findViewById(R.id.wind_value);
        wind = view.findViewById(R.id.wi_wind);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

        IntentFilter iF = new IntentFilter();

        // Read action when music player changed current song
        // I just try it with stock music player form android

        // stock music player
        iF.addAction("com.android.music.metachanged");

        // MIUI music player
        iF.addAction("com.miui.player.metachanged");

        // HTC music player
        iF.addAction("com.htc.music.metachanged");

        // WinAmp
        iF.addAction("com.nullsoft.winamp.metachanged");

        // MyTouch4G
        iF.addAction("com.real.IMP.metachanged");

        iF.addAction("com.google.android.music");

        getActivity().registerReceiver(mReceiver, iF);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver,
                new IntentFilter(Constants.TICK));

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.android.music.musicservicecommand");

                if(toggle == 0){
                    if(first){
                        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY);
                        mAudioManager.dispatchMediaKeyEvent(event);
                        play.setBackgroundResource(R.drawable.music_pause);
                        first = false;
                    }else{
                        //KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY);
                        //mAudioManager.dispatchMediaKeyEvent(event);
                        i.putExtra("command", "play");
                        getActivity().sendBroadcast(i);
                    }
                    play.setBackgroundResource(R.drawable.music_pause);
                }else{
                    play.setBackgroundResource(R.drawable.music_play);
                    //KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PAUSE);
                    //mAudioManager.dispatchMediaKeyEvent(event);

                    i.putExtra("command", "pause");
                    getActivity().sendBroadcast(i);
                }
                toggle = 1 - toggle;
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_NEXT);
                //mAudioManager.dispatchMediaKeyEvent(event);
                Intent i = new Intent("com.android.music.musicservicecommand");
                i.putExtra("command", "next");
                getActivity().sendBroadcast(i);
                play.setBackgroundResource(R.drawable.music_pause);
                toggle = 1;
            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PREVIOUS);
                //mAudioManager.dispatchMediaKeyEvent(event);
                Intent i = new Intent("com.android.music.musicservicecommand");
                i.putExtra("command", "previous");
                getActivity().sendBroadcast(i);
                play.setBackgroundResource(R.drawable.music_pause);
                toggle = 1;
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        Objects.requireNonNull(getActivity()).unregisterReceiver(mReceiver);
        Objects.requireNonNull(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void beforeTaskStarts(String s) {
    }

    @Override
    public void onTaskCompleted(String s) {
        JSONObject root = null;
        try {
            starttxt.setVisibility(View.GONE);
            Log.d(TAG, s);
            root = new JSONObject(s);
            String name = root.getString("name");
            JSONArray weather = root.getJSONArray("weather");
            double temp =
                    root.getJSONObject("main").getDouble("temp");
            double mint =
                    root.getJSONObject("main").getDouble("temp_min");
            double maxt =
                    root.getJSONObject("main").getDouble("temp_max");
            int pressure =
                    root.getJSONObject("main").getInt("pressure");
            int hum =
                    root.getJSONObject("main").getInt("humidity");
            double wind =
                    root.getJSONObject("wind").getDouble("speed");

            int weatherId = weather.getJSONObject(0).getInt("id");
            //Log.d(TAG, "City " + name + " temp " + temp + " weather " +
            // weather.getJSONObject(0).getString("main"));
            temp-=273.15;
            int tmp = (int) Math.round(temp);
            String tmpT = tmp+"";
            this.temp.setText(tmpT);
            this.temp.setVisibility(View.VISIBLE);
            this.cels.setVisibility(View.VISIBLE);
            mint-=273.15;
            int tmp1 = (int) Math.round(mint);
            String tmpT1 = tmp1+"";
            this.tempminv.setText(tmpT1);
            this.tempminv.setVisibility(View.VISIBLE);
            this.tempmin.setVisibility(View.VISIBLE);
            maxt-=273.15;
            int tmp2 = (int) Math.round(maxt);
            String tmpT2 = tmp2+"";
            Log.d(TAG, "Raw " + maxt + " processed " + tmp2);
            this.tempmaxv.setText(tmpT2);
            this.tempmaxv.setVisibility(View.VISIBLE);
            this.tempmax.setVisibility(View.VISIBLE);
            this.pressurev.setText(pressure + " hPa");
            this.pressurev.setVisibility(View.VISIBLE);
            this.pressure.setVisibility(View.VISIBLE);
            this.humidityv.setText(hum+"%");
            this.humidityv.setVisibility(View.VISIBLE);
            this.humidity.setVisibility(View.VISIBLE);
            this.windv.setText(wind + " m/s");
            this.windv.setVisibility(View.VISIBLE);
            this.wind.setVisibility(View.VISIBLE);
            this.city.setText(name);
            this.city.setVisibility(View.VISIBLE);
            if(weatherId>=200 && weatherId<=232){
                icon.setImageResource(R.drawable.ic_wi_thunderstorm);
            }else if(weatherId>=300 && weatherId<=321){
                icon.setImageResource(R.drawable.ic_wi_shower_rain);
            }else if(weatherId>=500 && weatherId<=504){
                icon.setImageResource(R.drawable.ic_wi_rain);
            }else if(weatherId>=511 && weatherId<=531){
                icon.setImageResource(R.drawable.ic_wi_shower_rain);
            }else if(weatherId>=600 && weatherId<=622){
                icon.setImageResource(R.drawable.ic_wi_snow);
            }else if(weatherId>=701 && weatherId<=781) {
                icon.setImageResource(R.drawable.ic_wi_mist);
            }else if(weatherId == 800){
                icon.setImageResource(R.drawable.ic_weather_sunny);
            }else if(weatherId == 801){
                icon.setImageResource(R.drawable.ic_wi_few_clouds);
            }else if(weatherId == 802){
                icon.setImageResource(R.drawable.ic_wi_scattered_clouds);
            }else if(weatherId == 803 || weatherId == 804){
                icon.setImageResource(R.drawable.ic_wi_broken_clouds);
            }
            icon.setVisibility(View.VISIBLE);



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
