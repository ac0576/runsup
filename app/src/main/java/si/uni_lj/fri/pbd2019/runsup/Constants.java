package si.uni_lj.fri.pbd2019.runsup;


public class Constants {
    public static String TICK = "si.uni_lj.fri.pbd2019.runsup.TICK";
    public static String START = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START";
    public static String CONTINUE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE";
    public static String PAUSE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE";
    public static String STOP = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP";
    public static String DEAD = "si.uni_lj.fri.pbd2019.runsup.DEAD";

    public static String SELECT_SPOT = "si.uni_lj.fri.pdb2019.runsup.UPDATE_SPORT_ACTIVITY";
    public static String SHARED_USER = "si.uni_lj.fri.pbd2019.runsup.user";
    //public static String SHARED_USER_LOGGED = "userSignedIn";
    public static String SHARED_USER_ID = "userId";
    public static String SHARED_USER_PIC = "si.uni_lj.fri.pbd2019.runsup.user_pic";
    public static String SHARED_USER_UID = "si.uni_lj.fri.pbd2019.runsup.userID";
    public static String SHARED_USER_WEIGHT = "si.uni_lj.fri.pbd2019.runsup.user_height";
    public static String SHARED_USER_HEIGHT = "si.uni_lj.fri.pbd2019.runsup.user_weight";
    public static String SHARED_USER_AGE = "si.uni_lj.fri.pbd2019.runsup.user_age";
    public static String SHARED_USER_CRASH = "si.uni_lj.fri.pbd2019.runsup.crash";

    public static String MUSIC_SONG = "song";
    public static String MUSIC_ARTIST = "artist";
    public static String MUSIC_ALBUM = "album";


    public static int STATE_STOPPED = 0;
    public static int STATE_RUNNING = 1;
    public static int STATE_PAUSED = 2;
    public static int STATE_CONTINUE = 3;

    public static String STOPWATCH_TAG = "fragment_stopwatch";
    public static String HISTORY_TAG = "fragment_history";
    public static String SETTINGS_TAG = "fragment_settings";
    public static String ABOUT_TAG = "fragment_about";
}
