package si.uni_lj.fri.pbd2019.runsup;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class GetWeather extends AsyncTask<Object, Void, String> {
    private TaskListener mListener;
    private static final String TAG = "FetchDataTask";

    public GetWeather(TaskListener tl){
        mListener = tl;
    }


    @Override
    protected String doInBackground(Object... objects) {
        Log.d(TAG, "Async started.");

        URL url = null;
        InputStream is = null;

        LatLng sw = (LatLng) objects[0];

        String baseurl = "https://api.openweathermap.org/data/2.5/weather";
        String charset = "UTF-8";
        String lat = ""+sw.latitude;
        String lng = ""+sw.longitude;
        String apikey = "a4b30d74e0ad69d26c31335ac596bf58";

        HttpsURLConnection conn = null;
        try {
            String query = String.format("lat=%s&lon=%s&appid=%s",
                    URLEncoder.encode(lat, charset),
                    URLEncoder.encode(lng, charset),
                    URLEncoder.encode(apikey, charset));
            url = new URL(baseurl + "?" + query);

            conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();

            is = conn.getInputStream();

            StringBuilder fullString = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                fullString.append(line);
            }
            reader.close();

            return fullString.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }   finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }



    public interface TaskListener {
        void beforeTaskStarts(String s);
        void onTaskCompleted(String s);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mListener.onTaskCompleted(s);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mListener.beforeTaskStarts("");
    }
}
