# RunsUp
Changes done for the 4th sprint:

Stopwatch has been replaced with CollectionFragment along with FragmentStatePagerAdapter which include WidgetFragment, StopwatchFragment and ActiveWorkoutMapFragment positioned in the following order and a user can easily navigate through the fragments by swiping left or right.

ActiveWorkoutMapMap and the button in StopwatchFragment to start ActiveWorkoutMapMap  were removed and replaced with ActiveWorkoutMapFragment for better UX (I determined to be too difficult to hit a small button while running so now the user simply needs to left).

WidgetFragment contains 2 parts. The upper part is a music player which can be used to navigate through the users music without changing the application. The player works by receiving and broadcasting commands from and to the default music player the user uses (depending on the default player my app might not capture the specific player broadcast since i added a limited number of filters, I found that this works best with Google Play Music). The lower part is weather widget which takes the users coordinates and communicates with OpenWeatherMap API to display the weather conditions for the user so he can better prepare for his exercise.

Added in settings goal and milestone which the user can now enable/disable and set a distance for himself. The milestones work by whenever a user reaches a certain distance the app plays a ding sound for example every 1 km (this was meant as a form of encouragement or an achievement for the user) and goal is so that the user can set a goal for himself when the user reaches the assigned goal the user receives a notification telling the user that he finished his assigned workout.
